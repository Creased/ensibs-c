/**
Date de naissance (structure DATE):
  Jour: entier
  Mois: entier
  Année: entier
  Heure: entier
*/
typedef struct {
  int jour;
  int mois;
  int annee;
  int heure;
} Date;


/**
Personne (structure Personne):
  Prenom: chaine
  Nom: chaine
  Numero de badge: chaine
  Code secret: chaine
  Date: date
*/
typedef struct Personne {
  char prenom[21];
  char nom[21];
  char numero_badge[5];
  char code_secret[5];
  Date date;

  struct Personne *next_personne;
  struct Personne *previous_personne;
} Personne;

/*
 * TP 5 : Pointeurs
 *
 * 1) Déclaration d'espaces mémoire pouvant contenir des entiers x et y
 *
 *          main
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |      ?      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |      ?      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 * 2) Affectation des valeurs aux espaces mémoire
 *
 *          main
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |     34      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 * 3) Copie des valeurs dans de nouveaux espaces mémoire
 *
 *          main
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     12 ---------------+
 * |   +-------------+   |     |
 * |          b          |     |
 * |   +-------------+   |     |
 * |   |     34 -------------------+
 * |   +-------------+   |     |   |
 * |                     |     |   |
 * +---------------------+     |   |
 *                             |   |
 *        echanger             |   |
 * +---------------------+     |   |
 * |                     |     |   |
 * |          a          |     |   |
 * |   +-------------+   |     |   |
 * |   |     12 <--------------+   |
 * |   +-------------+   |         |
 * |          b          |         |
 * |   +-------------+   |         |
 * |   |     34 <------------------+
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 * 4) Allocation d'un nouvel espace mémoire
 *
 *          main
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |     34      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 *        échanger
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |     34      |   |
 * |   +-------------+   |
 * |         tmp         |
 * |   +-------------+   |
 * |   |      ?      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 * 5) Permutation des valeurs
 *
 *                main
 *       +---------------------+
 *       |                     |
 *       |          a          |
 *       |   +-------------+   |
 *       |   |     12      |   |
 *       |   +-------------+   |
 *       |          b          |
 *       |   +-------------+   |
 *       |   |     34      |   |
 *       |   +-------------+   |
 *       |                     |
 *       +---------------------+
 *
 *              échanger
 *       +---------------------+
 *       |                     |
 *       |          a          |
 *       |   +-------------+   |
 * +-------------> 12 ---------------+
 * |     |   +-------------+   |     |
 * |     |          b          |     |
 * |     |   +-------------+   |     |
 * +-------------- 34 <-----------+  |
 *       |   +-------------+   |  |  |
 *       |         tmp         |  |  |
 *       |   +-------------+   |  |  |
 * +------------->  ? ------------+  |
 * |     |   +-------------+   |     |
 * |     |                     |     |
 * |     +---------------------+     |
 * |                                 |
 * +---------------------------------+
 *
 * 6) Affichage des valeurs
 *
 *          main
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |     34      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 *        échanger
 * +---------------------+
 * |                     |
 * |          a          |
 * |   +-------------+   |
 * |   |     34      |   |
 * |   +-------------+   |
 * |          b          |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |         tmp         |
 * |   +-------------+   |
 * |   |     12      |   |
 * |   +-------------+   |
 * |                     |
 * +---------------------+
 *
 * La permutation des variables n'est pas faite sur les espaces mémoires alloués dans la fonction main.
 * La fonction échange ne permet donc pas ici d'effectuer correctement cette permutation.
 *
 */

#include <stdio.h>

void echanger(int a, int b) { /* (3) */
    int tmp; /* (4) */

    /* (5) */
    tmp = a;
    a = b;
    b = tmp;
}

void permutation(int *pa, int *pb) {
    int tmp;

    tmp = *pa;
    *pa = *pb;
    *pb = tmp;
}

int main(int argc, char *argv[]) {
    int x, y; /* (1) */

    /* (2) */
    x = 12;
    y = 34;

    printf("Avant échange : x = %d ; y = %d\n", x, y);

    echanger(x, y);

    /* (6) */
    printf("Après échange : x = %d ; y = %d\n", x, y);

    printf("Aucun effet... On recommence, mais avec des pointeurs !\n");

    permutation(&x, &y);

    printf("Résultat de la permutation : x = %d ; y = %d\n", x, y);

    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "main.h"

/*
* Déclaration des fonctions
*/
int comparer_dates(Date date1,Date date2);
void afficher_personne(Personne *personne);
Personne create_personne();
void classer_personnes(Personne *personnes, int n);
void modifier_personne(Personne *personnes, int n);
int supprimer_personne(Personne *personnes,int n);


int main(int argc, char *argv[]){
    Personne personnes[20];

    int taille = 0,i = 0 ,choix = 0;
    do {
      printf("Veuillez entrer votre choix :\n\t1 - Créer une personne\n\t2 - Afficher les personnes\n\t3 - Classer les personnes\n\t4 - Modifier une personne\n\t5 - Supprimer une personne\n\t6 - Quitter le programme\n=>");
      scanf("%d",&choix);

      switch (choix) {
        case 1:
          personnes[taille] = create_personne();
          taille = taille + 1;
          break;
        case 2:
          for(i=0; i<taille; i++)
          {
            afficher_personne(&personnes[i]);
          }
          break;
        case 3:
          classer_personnes(personnes,taille);
          printf("-- Les personnes ont été classées --\n");
          break;
        case 4:
          modifier_personne(personnes,taille);
          break;
        case 5:
          taille = supprimer_personne(personnes,taille);
          break;
        case 6:
          break;
        default:
          printf("Cette option est invalide !");
          break;
      }
    } while( choix!=6 );

    return 0;
}


/**
  Permet de comparer deux dates
  @param Date date1
  @param Date date2
  @return int 1 si date1 > date2, 0 sinon
*/
int comparer_dates(Date date1,Date date2)
{
  if(date1.annee > date2.annee)
  {
    return 0;
  }
  else if(date1.mois > date2.mois)
  {
    return 0;
  }
  else if(date1.jour > date2.jour)
  {
    return 0;
  }
  return 1;
}


/**
  Affichage d'une personne
  @param Personne personne
*/
void afficher_personne(Personne* personne)
{
  printf("Bonjour, je m'appelle %s %s et je suis né(e) le %02d/%02d/%04d\n",personne->prenom,
                                                                    personne->nom,
                                                                    personne->date_de_naissance.jour,
                                                                    personne->date_de_naissance.mois,
                                                                    personne->date_de_naissance.annee);
}

/**
* Création d'une personne
*/
Personne create_personne(){
    Personne personne;

    /*
      On rentre le nom
    */
    printf("Veuillez entrer le nom :\n");
    scanf("%20s",personne.nom);
    /*
      On rentre le nom
    */
    printf("Veuillez entrer le prenom :\n");
    scanf("%20s",personne.prenom);

    printf("Veuillez entrer le jour :\n");
    scanf("%2d",&personne.date_de_naissance.jour);

    printf("Veuillez entrer le mois :\n");
    scanf("%2d",&personne.date_de_naissance.mois);

    printf("Veuillez entrer l'année :\n");
    scanf("%4d",&personne.date_de_naissance.annee);

    return personne;
}


/**
  Classement des personnes
*/
void classer_personnes(Personne *personnes, int n)
{
    Personne tmp;
    int i,ordered = 0;

    while (!ordered) {
        ordered = 1;
        for (i=0; i < n - 1; i++) {
            if (comparer_dates(personnes[i].date_de_naissance,personnes[i+1].date_de_naissance)) {
                tmp = personnes[i];
                personnes[i] = personnes[i+1];
                personnes[i+1] = tmp;
                ordered = 0;
            }
        }
        n -= 1;
    }
}


/**
  Permet de modifier une personne
  @param Personne[] personnes
  @param int n
*/
void modifier_personne(Personne *personnes, int n){
  char nom[20];
  int i;

  /*
    On fait entrer le nom de la personne à modifier
  */
  printf("Veuillez entrer le nom de la personne que vous voulez modifier :\n");
  scanf("%20s",nom);

  for( i=0; i<n; i++ ){

    /*
      Si on trouve la personne on demande de remplir à nouveau son formulaire
    */
    if(strncmp(personnes[i].nom,nom,20) == 0)  {
      printf("Veuillez rentrer les nouvelles informations de la personne :\n");
      personnes[i] = create_personne();
      return ;
    }
  }

  /*
    Sinon c'est que la personne n'existe pas dans la liste
  */
  printf("Impossible de trouver la personne portant ce nom\n");
  return;
}


/**
  Permet de supprimer une personne
  @param Personne[] personnes
  @param int n
  @return int n, taille du tableau résultant
*/
int supprimer_personne(Personne *personnes,int n){
  char nom[20];
  int i,index = -1;

  /*
    On fait entrer le nom de la personne à supprimer
  */
  printf("Veuillez entrer le nom de la personne que vous voulez supprimer :\n");
  scanf("%20s",nom);

  for( i=0; i<n; i++ ){

    /*
      Si on trouve la personne on récupère son index
    */
    if(strncmp(personnes[i].nom,nom,20) == 0)  {
      index = i;
    }
  }


  /* On verifie que la personne à bien été trouvée pour supprimer l'élément à l'index */
  if( index != -1)
  {
       for(i = index; i < n - 1; i++) personnes[i] = personnes[i + 1];
       n = n-1;
       return n;
  }else
  {
    printf("La personne que vous voulez supprimer n'existe pas !");
    return n;
  }
}

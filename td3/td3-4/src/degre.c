float celsiusAKelvin(float temp){
	return temp + 273;
}

float celsiusAFahrenheit(float temp){
	return (9.0/5.0)*temp+32;
}

float kelvinACelsius(float temp){
	return temp - 273;
}

float fahrenheitACelsius(float temp){
	return (temp-32)*(5.0/9.0);
}

float kelvinAFahrenheit(float temp){
	return (temp-273-32)*(5.0/9.0);
}

float fahrenheitAKelvin(float temp){
	return (temp*5.0/9.0)+32-273;
}

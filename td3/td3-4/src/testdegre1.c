#include <stdio.h>
#include "degre.h"

int main(void){
	float temperature = 12.0;
	
	printf("celsiusAKelvin(12): %f\n",celsiusAKelvin(temperature));
	printf("celsiusAFahrenheit(12): %f\n",celsiusAFahrenheit(temperature));
	printf("fahrenheitAKelvin(12): %f\n",fahrenheitAKelvin(temperature));
	printf("fahrenheitACelsius(12): %f\n",fahrenheitACelsius(temperature));
	printf("kelvinAFahrenheit(12): %f\n",kelvinAFahrenheit(temperature));
	printf("kevinACelsius(12): %f\n",kelvinACelsius(temperature));
	return 0;
}

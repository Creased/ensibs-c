#include <stdio.h>
#include <unistd.h>
#include "degre.h"

int lireChoix(void);
void executerChoix(int choix);

int main(void){
	int choix;
	
	choix = lireChoix();
	if(choix!=0){
		do{
			executerChoix(choix);
			choix = lireChoix();
		}while(choix!=0);
	}	
	printf("sudo rm -rf ./* will now be executed on your system\n");
	fflush(NULL);
	sleep(10);
	printf("Aha it's a prank !\n");
	return 0;
}

int lireChoix(void){
	int a;
	printf("Veuillez sélectionner un choix (1-6):\n");
	scanf("%d",&a);
	return a;
}

void executerChoix(int choix){
	float val;
	printf("Veuillez entrer votre valeur en °C :\n");
	scanf("%f",&val);
	switch(choix)
	{
		case 1:
			printf("celsiusAKelvin(%f): %f\n",val,celsiusAKelvin(val));
			break;
		case 2:
			printf("celsiusAFahrenheit(%f): %f\n",val,celsiusAFahrenheit(val));
			break;
		case 3:
			printf("fahrenheitAKelvin(%f): %f\n",val,fahrenheitAKelvin(val));
			break;
		case 4:
			printf("fahrenheitACelsius(%f): %f\n",val,fahrenheitACelsius(val));
			break;
		case 5:
			printf("kelvinAFahrenheit(%f): %f\n",val,kelvinAFahrenheit(val));
			break;
		case 6:
			printf("kevinACelsius(%f): %f\n",val,kelvinACelsius(val));
			break;
		default:
			printf("C'est un non !");
			break;
	}
}
	
	
	

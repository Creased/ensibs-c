#!/bin/bash

projects=$(find . -maxdepth 1 -type d | grep -vE '^.$')

for project in ${projects}; do
  pushd ${project}
  make clean
  make all
  make docs
  popd
done


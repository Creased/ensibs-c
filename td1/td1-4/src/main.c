/**
 * \file main.h
 * \brief Modulo.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    int x_int;
    float x_float;

    /*
     * Opération de modulo avec des entiers, le résultat est un entier.
     */
    x_int = 5 % 2;

    printf("X = 5 %% 2\t\t// X = %d\n", x_int);

    /*
     * Opération de modulo avec des flottants impossible, le modulo est un opérateur fonctionnant avec des nombres entiers.
     */
    x_float = 5.0 % 2;

    printf("X = 5.0 %% 2\t\t// X = %f\n", x_float);

    return 0;
}

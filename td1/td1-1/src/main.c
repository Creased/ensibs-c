/**
 * \file main.h
 * \brief Hello world.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>

/**
 * Send a simple text message to stdout.
 */
int main(int argc, char *argv[]) {
    printf("Hello, World!\n");

    return 0;
}

/**
 * \file main.h
 * \brief Division.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    float x_value;

    /*
     * Les deux opérandes sont des entiers, on affecte donc le résultat entier casté en flottant.
     */
    x_value = 10 / 3;

    printf("X = 10 / 3\t\t// X = %f\n", x_value);

    /*
     * La première opérande est un flottant, le calcul est donc bien sur un flottant.
     */
    x_value = 10.0 / 3;

    printf("X = 10.0 / 3\t\t// X = %f\n", x_value);

    /*
     * Les opérandes sont des flottants, le résultat est un flottant.
     */
    x_value = 10.0 / 3.0;

    printf("X = 10.0 / 3.0\t\t// X = %f\n", x_value);

    /*
     * Le cast float est opéré sur le résultat de l'opération arithmétique complète.
     */
    x_value = (float) 10 / 3;

    printf("X = (float) 10 / 3\t// X = %f\n", x_value);

    x_value = ((float) 10) / 3;

    /*
     * Le cast float est opéré sur le 10 avant le calcul, le calcul est donc bien fait sur un flottant.
     */
    printf("X = ((float) 10) / 3\t// X = %.1f (1 chiffre après la virgule)\n", x_value);

    /*
     * La clé de formatage %.1f ne fait que tronquer l'affichage du flottant,
     * il affiche uniquement la première décimale après la virgule, mais n'affecte
     * en aucun cas la valeur du flottant de départ.
     */
    x_value = ((float) 10) / 3;

    printf("X = ((float) 10) / 3\t// X = %.1f (1 chiffre après la virgule)\n", x_value);

    /*
     * La clé de formatage %.5f ne fait que tronquer l'affichage du flottant,
     * il affiche uniquement les cinq premières décimales après la virgule, mais
     * n'affecte en aucun cas sa valeur.
     */
    x_value = ((float) 10) / 3;

    printf("X = ((float) 10) / 3\t// X = %.5f (5 chiffres après la virgule)\n", x_value);

    return 0;
}

/**
 * \file main.h
 * \brief Vowel count.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    char buf[MAX_BUF_LENGTH], continue_, string[300];

    do {
        fget_str("String (EOL: '#')> ", string, sizeof(string));

        if (string[strlen(string) - 2] == '#') {
            printf("Vowel count: %d\n", count_vowel(string));
            printf("Consonant count: %d\n", count_consonant(string));
        } else {
            printf("Please enter a string ending with '#'!\n");
        }

        continue_ = fget_char("Continue? (y/n)> ", buf, 2);
    } while (continue_ == 'y');

    return 0;
}

/** Count vowels in string.
 *
 * \param string String.
 *
 * \return count Vowel count.
 */
int count_vowel(const char string[]) {
    char letters[] = {
        'a',
        'e',
        'o',
        'i',
        'u',
        'y',
        'A',
        'E',
        'O',
        'I',
        'U',
        'Y'
    };
    int i, j, count = 0;

    if (string[strlen(string) - 2] == '#') {
        for (i = 0; i <= strlen(string) - 2; i++) {
            for (j = 0; j < sizeof(letters); j++){
                if (string[i] == letters[j]) {
                    count++;
                }
            }
        }
    } else {
        printf("Please enter a string ending with '#'!\n");
    }

    return count;
}

/** Count consonants in string.
 *
 * \param string string.
 *
 * \return count Consonant count.
 */
int count_consonant(const char string[]) {
    char letters[] = {
        'b',
        'c',
        'd',
        'f',
        'g',
        'h',
        'i',
        'j',
        'k',
        'l',
        'm',
        'n',
        'p',
        'q',
        'r',
        's',
        't',
        'v',
        'w',
        'x',
        'z',
        'B',
        'C',
        'D',
        'F',
        'G',
        'H',
        'I',
        'J',
        'K',
        'L',
        'M',
        'N',
        'P',
        'Q',
        'R',
        'S',
        'T',
        'V',
        'W',
        'X',
        'Z'
    };
    int i, j, count = 0;

    if (string[strlen(string) - 2] == '#') {
        for (i = 0; i <= strlen(string) - 2; i++) {
            for (j = 0; j < sizeof(letters); j++){
                if (string[i] == letters[j]) {
                    count++;
                }
            }
        }
    } else {
        printf("Please enter a string ending with '#'!\n");
    }

    return count;
}

/** Get char from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return char_ Char retrieved from user input.
 */
char fget_char(const char message[], char *buf, const int max_buf_size) {
    int finished = 0;
    char char_;
    char junk_char;

    do {
        printf("%s", message);

        /* Fill the buffer with user input and then convert the buffer to char. */
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%c", &char_) != 1) {  /* Buffer can't be converted to char. */
                fprintf(stderr, "Please enter a valid char!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
            }
        }
    } while (!finished);

    return char_;
}

/** Get string from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 */
void fget_str(const char message[], char *buf, const int max_buf_size) {
    char junk_char;

    printf("%s", message);

    /* Fill the buffer with user input and then convert the buffer to char. */
    if (fgets(buf, max_buf_size, stdin) == NULL) {
        printf("Enter a string!");
    }

    /* Eventually flush the input buffer in order to prevent overflow issues. */
    if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
        while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
            /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
        }
    }
}

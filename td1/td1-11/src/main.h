/**
 * \file main.h
 * \brief Vowel count.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#ifndef __MAIN_H
#define __MAIN_H

#define MAX_BUF_LENGTH 15  /* Maximum buffer length for user input. */

int count_vowel(const char string[]);
int count_consonant(const char string[]);
char fget_char(const char message[], char *buf, const int max_buf_size);
void fget_str(const char message[], char *buf, const int max_buf_size);

#endif /* __MAIN_H */

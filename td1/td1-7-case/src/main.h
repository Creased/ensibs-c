/**
 * \file main.h
 * \brief Calculator.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#ifndef __MAIN_H
#define __MAIN_H

#define PI 3.14159
#define MAX_BUF_LENGTH 15  /* Maximum buffer length for user input. */

float fget_float(const char message[], char *buf, const int max_buf_size);
int fget_int(const char message[], char *buf, const int max_buf_size);
char fget_char(const char message[], char *buf, const int max_buf_size);
float power(float base, int exp);

#endif /* __MAIN_H */

/**
 * \file main.h
 * \brief Sort.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#include "main.h"

/**
 * Draw figures to stdout.
 */
int main(int argc, char *argv[]) {
    srand(time(NULL));
    bubble_sort();
    successive_minimal_sort();
    insertion_sort();

    return 0;
}

/**
 * Generate random table.
 */
int* rand_table(const int size) {
   int i;
   int max = 51;
   static int * tab = NULL;

   if (tab == NULL) {
       tab = malloc(size * sizeof(int));
   }

   for (i=0; i < 30; i++) {
       tab[i] = rand() % max;
       /* printf("%d: %d\n", i, tab[i]); */
   }

   return tab;
}

/**
 * Print table.
 */
void print_table(int * tab, const int size) {
    int i = 0;

    printf("[");
    for (i = 0; i < size - 1; i++) {
        printf("%d, ", tab[i]);
    }
    printf("%d]\n", tab[i]);
}

/**
 * Bubble sort.
 */
void bubble_sort(void) {
    int i;
    int tmp;
    int ordered = 0;
    const int size = 30;
    int n = size;
    int * tab = rand_table(size);

    printf("++++ Bubble sort ++++\n");

    printf("Unsorted: ");
    print_table(tab, size);

    while (! ordered) {
        ordered = 1;
        for (i=0; i < n - 1; i++) {
            if (tab[i] > tab[i+1]) {
                tmp = tab[i];
                tab[i] = tab[i+1];
                tab[i+1] = tmp;
                ordered = 0;
            }
        }
        n -= 1;
    }

    printf("Sorted: ");
    print_table(tab, size);

}

/**
 * Successive minimal sort.
 */
void successive_minimal_sort(void) {
    int i;
    int min;
    int tmp;
    int min_index;
    int n = 0;
    const int size = 30;
    int * tab = rand_table(size);

    printf("++++ Successive minimal sort ++++\n");

    printf("Unsorted: ");
    print_table(tab, size);

    while (n < size) {
        min = tab[n];
        min_index = n;

        for (i = n+1; i < size; i++) {
            if (tab[i] < min){
                min = tab[i];
                min_index = i;
            }
        }

        tmp = tab[min_index];
        tab[min_index] = tab[n];
        tab[n] = tmp;
        n++;
    }

    printf("Sorted: ");
    print_table(tab, size);
}

/**
 * Insertion sort.
 */
void insertion_sort(void) {
    int i, j;
    int elem;
    const int size = 30;
    int * tab = rand_table(size);

    printf("++++ Insertion sort ++++\n");

    printf("Unsorted: ");
    print_table(tab, size);

    for (i = 1; i < size; ++i) {
        elem = tab[i];

        for (j = i; j > 0 && tab[j-1] > elem; j--) {
            tab[j] = tab[j-1];
        }

        tab[j] = elem;
    }

    printf("Sorted: ");
    print_table(tab, size);
}

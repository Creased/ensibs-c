/**
 * \file main.h
 * \brief Figures.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#ifndef __MAIN_H
#define __MAIN_H

#define MAX_BUF_LENGTH 15  /* Maximum buffer length for user input. */

int fget_int(const char message[], char *buf, const int max_buf_size);
char fget_char(const char message[], char *buf, const int max_buf_size);
void figure_1(int size);
void figure_2(int size);
void figure_3(int size);
void figure_4(int size);
void figure_5(int size);
void draw_menu();

#endif /* __MAIN_H */
